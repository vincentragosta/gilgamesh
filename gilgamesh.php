<?php
/*
Plugin Name: Gilgamesh
Description: An anime-themed backend framework built on top of WordPress (Gutenberg)
Version: 1.0
License: GPL-2.0+
*/

use Gilgamesh\Controller;
use Gilgamesh\Service\RegisterConfigService;
use Gilgamesh\Service\RegisterExtensionService;

defined('ABSPATH') || exit;
if (! defined('USE_COMPOSER_AUTOLOADER') || ! USE_COMPOSER_AUTOLOADER) {
    require __DIR__ . '/vendor/autoload.php';
}

/**
 * Class Gilgamesh
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Gilgamesh
{
    const EXTENSIONS = [
        Controller\AfterSavePostController::class,
        Controller\FactoryController::class
    ];

    public function __construct()
    {
        add_action('plugins_loaded', function() {
            do_action('gilgamesh/init');
            add_action('init', function() {
                (new RegisterExtensionService(static::EXTENSIONS))->run();
                (new RegisterConfigService())->run();
            });
        });
    }
}

new Gilgamesh();