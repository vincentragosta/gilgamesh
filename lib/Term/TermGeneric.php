<?php

namespace Gilgamesh\Term;

/**
 * Class TermGeneric
 * @package Gilgamesh\Term
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class TermGeneric extends Term
{
    public function __construct($term, $taxonomy = null)
    {
        if ($taxonomy) {
            $term = get_term($term, $taxonomy);
        }
        parent::__construct($term);
    }
}
