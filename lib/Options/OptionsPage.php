<?php

namespace Gilgamesh\Options;

/**
 * Class OptionsPage
 * @package Gilgamesh\Options
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $parent
 */
class OptionsPage
{
    const OPTION_NAME = '';
    const PAGE_TITLE = '';

    protected $parent = 'options-general.php';

    public function register()
    {
        if (function_exists('fm_register_submenu_page') && !empty(static::OPTION_NAME) && !empty(static::PAGE_TITLE)) {
            try {
                fm_register_submenu_page(static::OPTION_NAME, $this->getParent(), static::PAGE_TITLE);
            } catch(\Exception $e) {
                error_log($e->getMessage());
            }
        }
    }

    public function getParent()
    {
        return $this->parent;
    }

    public static function getLayeredOption($group, $inner_group, $key = '')
    {
        if (empty($settings = get_option(static::OPTION_NAME))) {
            return '';
        }
        if (empty($group)) {
            return $settings;
        }
        if (empty($group_settings = $settings[$group])) {
            return '';
        }
        if (empty($inner_group)) {
            return $settings;
        }
        if (empty($inner_group_settings = $group_settings[$inner_group])) {
            return $group_settings;
        }
        if (empty($key)) {
            return $inner_group_settings;
        }
        return $inner_group_settings[$key];
    }

    public static function getOption($group, $key = '')
    {
        if (empty($settings = get_option(static::OPTION_NAME))) {
            return '';
        }
        if (empty($group)) {
            return $settings;
        }
        if (empty($group_settings = $settings[$group])) {
            return '';
        }
        if (empty($key)) {
            return $group_settings;
        }
        return $group_settings[$key];
    }
}
