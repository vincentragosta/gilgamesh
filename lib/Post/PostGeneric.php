<?php

namespace Gilgamesh\Post;

/**
 * Class PostGeneric
 * @package Gilgamesh\Post
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PostGeneric extends Post
{
    protected function isValidPostInit()
    {
        return true;
    }
}
