<?php

namespace Gilgamesh\Post;

use Gilgamesh\Image;
use Gilgamesh\Support\DateTime;
use Gilgamesh\Utility\ClassUtility;
use Gilgamesh\Utility\StringUtility;

/**
 * Class Post
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $ID
 * @property string $post_title
 * @property string $post_status
 * @property string $post_name
 * @property string $post_excerpt
 * @property string $post_content
 */
abstract class Post
{
    const POST_TYPE = null;
    private $_post_init = null;
    private $_post;
    private $_author;
    private $_fields = [], $_terms = [];
    protected $_permalink, $_featured_image;
    protected static $default_query = [];
    protected $_default_date_format;

    function __construct($post = null)
    {
        $this->_post_init = $post;
        $this->init();
    }

    protected function init()
    {

    }

    public function reset($reload = false)
    {
        $post_id = $this->post()->ID;
        $this->_post = $this->_author = $this->_permalink = $this->_featured_image = null;
        $this->_fields = $this->_terms = [];
        $this->_post_init = $post_id;
        if ($reload) {
            $this->post();
            $this->allTermIdsByTaxonomy();
            $this->permalink();
            $this->featuredImage();
        }
        $this->init();
    }

    public function permalink()
    {
        if (empty($this->_permalink)) {
            $this->_permalink = get_permalink($this->post());
        }

        return $this->_permalink;
    }

    public function publishedDate($default_format = null)
    {
        $Date = null;
        $format = $default_format ?: $this->_default_date_format;
        try {
            $Date = new DateTime($this->post()->post_date, $format);
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
        return $Date;
    }

    public function modifiedDate($default_format = null)
    {
        $Date = null;
        $format = $default_format ?: $this->_default_date_format;
        try {
            $Date = new DateTime($this->post()->post_modified, $format);
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
        return $Date;
    }

    public function featuredImage()
    {
        if (!isset($this->_featured_image) && class_exists('Gilgamesh\Image')) {
            $this->_featured_image = Image::get_featured($this->post());
        }
        return $this->_featured_image;
    }

    public function setFeaturedImage(Image $image)
    {
        $this->_featured_image = $image;
    }

    public function post()
    {
        if (empty($this->_post)) {
            $this->_post = $this->hasValidPostInit() ?
                get_post($this->_post_init) :
                (object)['ID' => null, 'post_type' => static::POST_TYPE];
            if (!$this->isValidPostInit()) {
                throw new \InvalidArgumentException(sprintf(
                    'Invalid post initialization for post type "%s" with id: %d',
                    static::POST_TYPE,
                    $this->_post->ID
                ));
            }
            $this->_post_init = null;
        }

        return $this->_post;
    }

    protected function isValidPostInit()
    {
        return $this->_post->post_type == static::POST_TYPE;
    }

    public function title()
    {
        return get_the_title($this->post());
    }

    public function content($use_global = true)
    {
        $content = $this->isGlobal() && $use_global ?
            get_the_content() :
            $this->post()->post_content;
        return apply_filters('the_content', $content);
    }

    public function type()
    {
        return static::POST_TYPE ?: $this->post()->post_type;
    }

    public function isGlobal()
    {
        return isset($GLOBALS['post']) && $GLOBALS['post'] == $this->post();
    }

    public function author()
    {
        if (empty($this->_author)) {
            $post = $this->post();
            if (!$post) return false;
            $this->_author = new \WP_User($post->post_author);
        }
        return $this->_author;
    }

    public function setAuthor(\WP_User $user)
    {
        $this->_author = $user;
        $this->post()->post_author = $user->ID;
    }

    public function terms($taxonomy)
    {
        if (empty($this->_terms[$taxonomy])) {
            $terms = get_the_terms($this->post(), $taxonomy);
            if (!is_array($terms)) {
                $terms = [];
            }
            $this->_terms[$taxonomy] = $terms;
        }
        return $this->_terms[$taxonomy];
    }

    public function allTermIdsByTaxonomy()
    {
        $taxonomies = get_object_taxonomies($this->type(), 'objects');
        foreach ($taxonomies as $name => &$term_ids) {
            $term_ids = array_map(function (\WP_Term $term) {
                return $term->term_id;
            }, $this->terms($name));
        }
        return $taxonomies;
    }

    public function isValid()
    {
        return $this->post() instanceof \WP_Post;
    }

    protected static function getQuery($args = [])
    {
        $defaults = [
            'post_type' => static::POST_TYPE,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'offset' => 0,
            'suppress_filters' => true
        ];
        if (current_user_can('read_private_posts')) {
            $defaults['post_status'] = ['publish', 'private'];
        }
        $defaults = wp_parse_args(static::getDefaultQuery(), $defaults);
        return wp_parse_args($args, $defaults);
    }

    public static function getDefaultQuery()
    {
        return static::$default_query;
    }

    public static function getPosts($args = [])
    {
        $args = static::getQuery($args);
        $query = new \WP_Query();
        $posts = $query->query($args);
        $ret = [];
        foreach ($posts as $post_obj) {
            $ret[] = static::create($post_obj);
        }
        return $ret;
    }

    public static function create($post_obj)
    {
        return new static($post_obj);
    }

    public static function createFromGlobal()
    {
        return static::create($GLOBALS['post']);
    }

    public function fields($fetch = true) {
        if ($fetch) {
            $fields = get_post_meta($this->post()->ID);
            foreach ((array) $fields as $key => $value) {
                $this->$key;
            }
        }

        return $this->_fields;
    }

    public function __get($name)
    {
        if ($method_name = ClassUtility::getMethodName($this, $name)) {
            if (!isset($this->_fields[$name])) {
                $value = $this->{$method_name}();
                $this->_fields[$name] = $value;
            }
            return $this->_fields[$name];
        }
        if (property_exists($post = $this->post(), $name)) {
            return $post->{$name};
        }

        if ($terms = $this->terms($this->getTaxonomyFromProperty($name))) {
            return $terms;
        }

        if (empty($this->_fields[$name])) {
            $field = get_post_meta($this->post()->ID, $name, true);
            if (in_array($name, $this->_date_field_names)) {
                $field = $field ? new DateTime($field, $this->_default_date_format) : null;
            }
            $this->_fields[$name] = $field;
        }

        return $this->_fields[$name];
    }

    public function __set($name, $value)
    {
        if ($method_name = ClassUtility::getMethodName($this, $name, 'set')) {
            return $this->{$method_name}($value);
        }
        if (property_exists('WP_Post', $name)) {
            $this->post()->{$name} = $value;
            return;
        }

        if ($taxonomy = $this->getTaxonomyFromProperty($name)) {
            $this->_terms[$taxonomy] = array_map(function ($value) use ($taxonomy) {
                return is_string($value) ?
                    get_term_by('slug', $value, $taxonomy) :
                    get_term($value, $taxonomy);
            }, (array)$value);
            return;
        }

        $this->_fields[$name] = $value;
    }

    public function __isset($name)
    {
        $value = $this->$name;
        return !empty($value);
    }

    private function getTaxonomyFromProperty($name)
    {
        if (taxonomy_exists($name)) {
            return $name;
        }

        $singular_name = StringUtility::singularize($name);
        return taxonomy_exists($singular_name) ? $singular_name : false;
    }

    private function hasValidPostInit()
    {
        return (
            is_numeric($this->_post_init) ||
            $this->_post_init instanceof \WP_Post
        );
    }
}
