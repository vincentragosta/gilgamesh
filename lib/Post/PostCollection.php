<?php

namespace Gilgamesh\Post;

use Gilgamesh\Object\ObjectCollection;

/**
 * Class PostCollection
 * @package Gilgamesh\Post
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PostCollection extends ObjectCollection
{
    protected static $object_class_name = Post::class;

    protected function getObjectHash($item)
    {
        return md5($item->ID);
    }

}
