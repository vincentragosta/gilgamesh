<?php

namespace Gilgamesh\Controller;

use Gilgamesh\Factory\PostFactory;
use Gilgamesh\Factory\TermFactory;

/**
 * Class FactoryController
 * @package Gilgamesh\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class FactoryController
{
    protected $post_type_models = [];
    protected $taxonomy_models = [];
    protected $data;

    function __construct()
    {
        add_action('init', [$this, 'initFactories'], 7);
    }

    public function initFactories()
    {
        $this->post_type_models = array_values(array_filter(apply_filters('gilgamesh/register_post_type_models', [])));
        foreach ($this->post_type_models as $model_class) {
            PostFactory::register($model_class);
        }
        $this->taxonomy_models = array_values(array_filter(apply_filters('gilgamesh/register_taxonomy_models', [])));
        foreach ($this->taxonomy_models as $model_class) {
            TermFactory::register($model_class);
        }
    }

}
