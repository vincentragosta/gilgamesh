<?php

namespace Gilgamesh\Controller;

/**
 * Class AfterSavePostController
 * @package Gilgamesh\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class AfterSavePostController
{
    public function __construct()
    {
        add_filter('update_postmeta', [$this, 'editLockAfterSavePost'], 10, 3);
    }

    public function editLockAfterSavePost($meta_id, $object_id, $meta_key)
    {
        if ($meta_key !== '_edit_lock') {
            return;
        }
        $post = get_post($object_id);
        do_action('after_save_post', $object_id);
        do_action("after_save_post_{$post->post_type}", $object_id);
    }
}
