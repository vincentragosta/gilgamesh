<?php

namespace Gilgamesh\Factory;

/**
 * Class Factory
 * @package Gilgamesh\Factory
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 */
abstract class Factory
{
    protected static $models = [];
    abstract static function create();
    abstract static function register($class);
}
