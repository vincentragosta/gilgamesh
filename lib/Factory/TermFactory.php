<?php

namespace Gilgamesh\Factory;

use Gilgamesh\Term\Term;
use Gilgamesh\Term\TermGeneric;

/**
 * Class TermFactory
 * @package Gilgamesh\Factory
 * @author Vincent Ragosta <vincentpasqualeragostaa@gmail.com>
 * @version 1.0
 */
class TermFactory extends Factory
{
    public static function create($term = null, $taxonomy = null)
    {
        if ($taxonomy) {
            $term = get_term($term, $taxonomy);
        }
        $model_class = static::$models[$term->taxonomy] ?? TermGeneric::class;
        return new $model_class($term);
    }

    public static function register($model_class)
    {
        if (!is_a($model_class, Term::class, true)) {
            throw new \InvalidArgumentException('Invalid term factory registration');
        }
        $taxonomy = $model_class::TAXONOMY;
        static::$models[$taxonomy] = $model_class;
    }
}
