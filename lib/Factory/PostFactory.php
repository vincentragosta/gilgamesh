<?php

namespace Gilgamesh\Factory;

use Gilgamesh\Post\Post;
use Gilgamesh\Post\PostGeneric;

/**
 * Class PostFactory
 * @package Gilgamesh\Factory
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PostFactory extends Factory
{
    const BASE_CLASS = Post::class;
    const DEFAULT_CLASS = PostGeneric::class;

    public static function create($post = null)
    {
        $post = get_post($post);
        $model_class = static::$models[$post->post_type] ?? static::DEFAULT_CLASS;
        return new $model_class($post);
    }

    public static function register($model_class)
    {
        if (!is_a($model_class, static::BASE_CLASS, true)) {
            throw new \InvalidArgumentException('Invalid post factory registration');
        }
        $post_type = $model_class::POST_TYPE;
        static::$models[$post_type] = $model_class;
    }
}
