<?php

namespace Gilgamesh\Repository;

/**
 * Interface ImmutableRepository
 * @package Gilgamesh\Repository
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
interface ImmutableRepository
{
    function findById($id);

    function findOne(array $query);

    function findAll();

    function find(array $query);

}
