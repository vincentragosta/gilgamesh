<?php

namespace Gilgamesh\Repository;

/**
 * Interface Repository
 * @package Gilgamesh\Repository
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
interface Repository extends ImmutableRepository
{
    function add($object);

    function remove($object);

}
