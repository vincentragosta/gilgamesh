<?php

namespace Gilgamesh\Utility;

/**
 * Class StringUtility
 * @package Gilgamesh\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class StringUtility
{
    public static function toCamelCase($symbol_name)
    {
        return lcfirst(static::toPascalCase($symbol_name));
    }

    public static function toPascalCase($symbol_name)
    {
        return str_replace('_', '', ucwords($symbol_name, '_'));
    }

    public static function toSnakeCase($symbol_name)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $symbol_name)), '_');
    }

    /**
     * Will singularize most words, but edge cases shouldn't rely on this for proper singularization.
     *
     * @param string $word
     *
     * @return string
     */
    public static function singularize($word)
    {
        if (0 !== strrpos($word, 's', -1)) return $word;
        if (0 === strrpos($word, 'es', -2)) {
            return substr($word, 0, -2);
        }
        return preg_replace('/ie$/', 'y', substr($word, 0, -1));
    }

    public static function pluralize($word)
    {
        $exploded = str_split(trim($word));
        $last = array_pop($exploded);
        switch ($last) {
            case 'y':
                $exploded[] = 'ies';
                break;
            case 's';
                $exploded[] = $last . 'es';
                break;
            default:
                $exploded[] = $last . 's';
                break;
        }

        return implode($exploded);
    }
}
