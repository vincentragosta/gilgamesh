<?php

namespace Gilgamesh\Utility;

/**
 * Class ClassExtension
 * @package Gilgamesh\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class ClassUtility
{
    public static function getMethodName($object, $name, $prefix = 'get')
    {
        $method_name = $prefix ? sprintf('%s_%s', $prefix, $name) : $name;
        if (method_exists($object, $method_name)) {
            return $method_name;
        }
        $camel_case_method_name = StringUtility::toCamelCase($method_name);
        if (method_exists($object, $camel_case_method_name)) {
            return $camel_case_method_name;
        }
        return false;
    }
}
