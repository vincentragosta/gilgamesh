<?php

namespace Gilgamesh\Utility;

/**
 * Class NavMenuUtility
 * @package Gilgamesh\Utility
 * @author Vincent Ragosta <vincenpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class NavMenuUtility
{
    const PRIMARY_NAVIGATION_LABEL = 'Primary Navigation';
    const PRIMARY_NAVIGATION_SLUG = 'primary_navigation';

    public static function hasNavMenu(string $slug)
    {
        return has_nav_menu($slug);
    }

    public static function getItems(string $label)
    {
        return wp_get_nav_menu_items($label);
    }

    public static function getPrimaryNavigationItems()
    {
        if (!static::hasNavMenu(static::PRIMARY_NAVIGATION_SLUG)) {
            return [];
        }
        return static::getItems(static::PRIMARY_NAVIGATION_LABEL);
    }
}