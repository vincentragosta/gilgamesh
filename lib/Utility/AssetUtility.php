<?php

namespace Gilgamesh\Utility;

/**
 * Class AssetUtility
 * @package Gilgamesh\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class AssetUtility
{
    public static function assetPath(string $path)
    {
        return sprintf('%s/%s', get_stylesheet_directory_uri() . '/assets', $path);
    }

    public static function buildPath(string $path)
    {
        return sprintf('%s/%s', get_stylesheet_directory_uri() . '/build', $path);
    }
}