<?php

namespace Gilgamesh\Utility;

use Gilgamesh\User\User;

/**
 * Class UserUtility
 * @package Gilgamesh\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class UserUtility
{
    public static function isValidUser($User)
    {
        return $User instanceof User;
    }

    public static function isSameUser($User, $UserToCompare)
    {
        if (!(static::isValidUser($User) || static::isValidUser($UserToCompare))) {
            return false;
        }
        return $User->ID === $UserToCompare->ID;
    }
}
