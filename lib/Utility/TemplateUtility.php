<?php

namespace Gilgamesh\Utility;

/**
 * Class TemplateUtility
 * @package Gilgamesh\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class TemplateUtility
{
    /**
     * @param string $file Path of template file.
     * @param array $scope Scope associative array - ['my_var' => 'hello world'] is extracted to $my_var = 'hello world'
     *                          inside the referenced template.
     *
     * @return null|string
     */
    public static function getFileContents($file, $scope = [])
    {
        if (!file_exists($file)) {
            return null;
        }
        if (is_object($scope)) {
            $scope = (array)$scope;
        }
        extract($scope, EXTR_SKIP);
        ob_start();
        include($file);

        return ob_get_clean();
    }

    /**
     * @param string|array $template_names Locate template (or first valid template file in array).
     * @param array $scope Scope associative array - ['my_var' => 'hello world'] is extracted to $my_var = 'hello world'
     *                                     inside the referenced template.
     * @param string|array $base_path A base path for the template path, assumes theme if not provided
     *
     * @return null|string
     */
    public static function getScoped($template_names, $scope = [], $base_path = '')
    {
        $file = static::locate($template_names, $base_path);
        return $file ? self::getFileContents($file, $scope) : null;
    }

    /**
     * Will try to locate first viable template file based on array of relative file paths against an array of base paths.
     * so ([file1, file2],[base1, base2]) will find the first match of base1/file1, base1/file2, base2/file1, etc.
     * If no base path is provided, assumes either full pathname or current theme directory.
     *
     * @param array|string $template_names
     * @param array|string $base_paths
     *
     * @return bool|string
     */
    public static function locate($template_names, $base_paths = '')
    {
        $default_extension = 'php';
        $extension_whitelist = [
            'php',
            'html',
            'blade'
        ];
        $template_names = is_array($template_names) ? array_filter($template_names) : [$template_names];
        $base_paths = is_array($base_paths) ? $base_paths : [$base_paths];
        $base_paths = array_filter($base_paths);
        $template_names = array_map(function ($name) use ($default_extension, $extension_whitelist) {
            $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

            return (in_array($ext, $extension_whitelist)) ? $name : $name . '.' . $default_extension;
        }, $template_names);

        foreach ($template_names as $template_name) {
            //if full file path already exists, use it
            if (file_exists($template_name)) {
                return $template_name;
            }
            //check in active theme folder
            if ($theme_template = locate_template($template_name)) {
                return $theme_template;
            }
            //then, try all supplied base paths
            foreach ($base_paths as $base_path) {
                $filename = implode('/', [rtrim($base_path, '/'), $template_name]);
                if (file_exists($filename)) {
                    return $filename;
                }
            }
        }
        return false;
    }
}
