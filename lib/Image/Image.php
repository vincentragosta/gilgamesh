<?php

namespace Gilgamesh;

/**
 * TODO: fix snake case methods!
 *
 * Class Image
 * @package Gilgamesh
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $url               Get the current URL of the image
 * @property int $width             Get the current width of the image
 * @property int $height            Get the current height of the image
 * @property bool $crop              Get the crop status of the image
 * @property string $orig_url          Get the original URL of the image
 * @property int $orig_width        Get the original width of the image
 * @property int $orig_height       Get the original height of the image
 * @property string $alt               Get the alt text from the WP alt field
 * @property string $title             Get the title from the WP title field
 * @property string $caption           Get the caption from the WP caption field
 * @property string $description       Get the description from the WP description field
 * @property string $css_class             Get the css class of the image
 * @property string $sizes             Get the sizes attribute
 *
 */
class Image
{
    protected static $upload_dirs, $upload_urls;
    protected static $upload_key = 'default';
    public $ID;
    private $_props = array();
    private $_el_attr = array();

    protected function __construct($attachment_id)
    {
        $this->ID = $attachment_id;
    }

    public function alt($alt_text)
    {
        $this->_props['alt'] = $alt_text;
        return $this;
    }

    public function width($new_width)
    {
        $this->_props['width'] = $new_width;
        return $this;
    }

    public function height($new_height)
    {
        $this->_props['height'] = $new_height;
        return $this;
    }

    public function crop($crop = true)
    {
        $this->_props['crop'] = $crop;
        return $this;
    }

    public function css_class($classes = '')
    {
        if (is_array($classes)) {
            $classes = implode(' ', $classes);
        }
        $this->_props['css_class'] = $classes;
        return $this;
    }

    public function custom_attr($key, $val)
    {
        $this->_el_attr[sanitize_title($key)] = $val;
        return $this;
    }

    public function attr($key)
    {
        return $this->_el_attr[sanitize_title($key)];
    }

    public function __invoke($size = null)
    {
        $this->build();
    }

    public function build()
    {
        $this->url;
        $this->resize();
    }

    public function get_html()
    {
        $this();
        $defaults = array(
            'src' => $this->url,
            'width' => $this->width,
            'height' => $this->height,
            'alt' => $this->alt,
        );
        if ($this->css_class) {
            $defaults['class'] = $this->css_class;
        }
        $ext = pathinfo($this->url, PATHINFO_EXTENSION);
        if ($ext == 'svg') {
            unset($defaults['width']);
            unset($defaults['height']);
        }
        $attributes = wp_parse_args($this->_el_attr, $defaults);
        return '<img ' . join(' ', array_map(function ($key) use ($attributes) {
                if (is_bool($attributes[$key])) {
                    return $attributes[$key] ? $key : '';
                }

                return $key . '="' . esc_attr($attributes[$key]) . '"';
            }, array_keys($attributes))) . ' />';
    }

    public function __toString()
    {
        return $this->get_html();
    }

    public function __get($attr)
    {
        if (!isset($this->_props[$attr])) {
            $this->pre_get($attr);
            if (!isset($this->_props[$attr])) {
                $this->get_from_meta($attr);
            }
        }
        return $this->_props[$attr];
    }

    private function pre_get($attr)
    {
        $sources = [
            'from_src' => ['url', 'width', 'height', 'orig_url', 'orig_width', 'orig_height'],
            'from_post' => ['title', 'caption', 'description'],
            'from_meta' => ['alt']
        ];
        foreach ($sources as $source => $attributes) {
            if (in_array($attr, $attributes)) {
                $func = [$this, 'get_' . $source];
                if (is_callable($func)) {
                    return call_user_func($func, $attr);
                }
            }
        }
    }

    private function get_from_src($attr)
    {
        $src = wp_get_attachment_image_src($this->ID, 'full');
        $fetched = [
            'url' => $src[0],
            'orig_url' => $src[0],
            'orig_width' => $src[1],
            'orig_height' => $src[2],
        ];
        $this->_props = wp_parse_args($this->_props, $fetched);
        $this->_props[$attr] = isset($this->_props[$attr]) ? $this->_props[$attr] : $this->_props['orig_' . $attr];
        return $this->_props[$attr];
    }

    private function get_from_post($attr)
    {
        $attachment = get_post($this->ID);
        $fetched = array(
            'title' => $attachment->post_title,
            'caption' => $attachment->post_excerpt,
            'description' => $attachment->post_content
        );
        $this->_props = wp_parse_args($this->_props, $fetched);
        return $this->_props[$attr];
    }

    private function get_from_meta($attr)
    {
        $key = ($attr == 'alt') ? '_wp_attachment_image_alt' : $attr;
        $this->_props[$attr] = get_post_meta($this->ID, $key, true);
        if ($attr == 'alt' && empty($this->_props[$attr])) {
            $this->_props[$attr] = $this->title;
        }
        return $this->_props[$attr];
    }

    private function hasBothDimensions()
    {
        return isset($this->_props['width']) && isset($this->_props['height']);
    }

    private function shouldCrop()
    {
        if (!isset($this->_props['crop'])) {
            $this->_props['crop'] = $this->hasBothDimensions();
        }
        return $this->hasBothDimensions() && $this->_props['crop'];
    }

    private function dimensionsAreUnchanged()
    {
        if (!(isset($this->_props['width']) || isset($this->_props['height']))) {
            $this->_props['width'] = $this->_props['orig_width'];
            $this->_props['height'] = $this->_props['orig_height'];
        }
        return $this->_props['width'] == $this->_props['orig_width'] && $this->_props['height'] == $this->_props['orig_height'];
    }

    private function isCroppingSmallerImage()
    {
        if (!$this->shouldCrop()) {
            return false;
        }
        return $this->_props['width'] > $this->_props['orig_width'] || $this->_props['height'] > $this->_props['orig_height'];
    }

    private function getNewDimensionsForSmallerImage()
    {
        $width_ratio = $this->_props['orig_width'] / $this->_props['width'];
        $height_ratio = $this->_props['orig_height'] / $this->_props['height'];
        $factor = min($width_ratio, $height_ratio);
        $new_w = round($this->_props['width'] * $factor);
        $new_h = round($this->_props['height'] * $factor);
        return [$new_w, $new_h];
    }

    private function getNewDimensionsForLargerImage()
    {
        $has_width = isset($this->_props['width']);
        $has_height = isset($this->_props['height']);
        $cropping = $this->shouldCrop();
        // Get desired ratio
        $desired_ratio = $cropping ?
            $this->_props['width'] / $this->_props['height'] :
            $this->_props['orig_width'] / $this->_props['orig_height'];
        $new_w = $new_h = 0;
        if ($has_width) {
            $new_w = $this->_props['width'];
            $new_h = $has_height ? $this->_props['height'] : round($new_w / $desired_ratio);
        } elseif ($has_height) {
            $new_h = $this->_props['height'];
            $new_w = round($new_h * $desired_ratio);
        }
        return $cropping ?
            [$new_w, $new_h] :
            $this->constrainDimensions($new_w, $new_h);
    }


    private function constrainDimensions($width, $height)
    {
        return wp_constrain_dimensions($this->_props['orig_width'], $this->_props['orig_height'], $width, $height);
    }

    private function getNewDimensions()
    {
        if ($this->dimensionsAreUnchanged()) {
            return false;
        }
        return $this->isCroppingSmallerImage() ?
            $this->getNewDimensionsForSmallerImage() :
            $this->getNewDimensionsForLargerImage();
    }

    private function getRelativePath($path, $ext)
    {
        $rel_path = str_replace(self::getUploadDir(), '', $path);
        return substr($rel_path, 0, -1 * (strlen($ext) + 1));
    }

    private function getResizedPath($path, $width, $height, $ext)
    {
        return sprintf('%s-%sx%s.%s', $path, $width, $height, $ext);
    }

    private static function setUploadPaths()
    {
        $upload_info = wp_upload_dir();
        static::$upload_dirs[static::$upload_key] = $upload_info['basedir'];
        static::$upload_urls[static::$upload_key] = $upload_info['baseurl'];
    }

    private static function getUploadDir()
    {
        if (empty(static::$upload_dirs[static::$upload_key])) {
            static::setUploadPaths();
        }
        return static::$upload_dirs[static::$upload_key];
    }

    private static function getUploadUrl()
    {
        if (empty(static::$upload_urls[static::$upload_key])) {
            static::setUploadPaths();
        }
        return static::$upload_urls[static::$upload_key];
    }

    private function resize()
    {
        if (!$new_dimensions = $this->getNewDimensions()) {
            return;
        }
        list($actual_w, $actual_h) = $new_dimensions;
        $path = get_attached_file($this->ID);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $rel_path = $this->getRelativePath($path, $ext);
        $resized_file = self::getUploadDir() . $this->getResizedPath($rel_path, $actual_w, $actual_h, $ext);
        if (!file_exists($resized_file)) {
            $resized = image_make_intermediate_size($path, $actual_w, $actual_h, $this->shouldCrop());
            if ($resized) {
                $actual_w = $resized['width'];
                $actual_h = $resized['height'];
            }
        }
        $url = self::getUploadUrl() . $this->getResizedPath($rel_path, $actual_w, $actual_h, $ext);

        $this->_props['url'] = apply_filters('wp_get_attachment_url', $url);
        // Update width and height attributes if no crop, since one might have changed
        if (!$this->_props['crop']) {
            $this->_props['width'] = $actual_w;
            $this->_props['height'] = $actual_h;
        }
    }

    public static function get_featured($post = null)
    {
        $current_post = get_post($post);
        if (empty($current_post)) {
            return null;
        }
        $attachment_id = get_post_thumbnail_id($current_post->ID);
        if (empty($attachment_id)) {
            return null;
        }

        $image = static::get_by_attachment_id($attachment_id);
        return $image;
    }

    public static function get_by_attachment_id($attachment_id)
    {
        $post_type = get_post_type($attachment_id);
        if ('attachment' === $post_type) {
            return new static($attachment_id);
        }
        return false;
    }

    public static function create_from_url($url, $post_id = 0)
    {
        require_once ABSPATH . 'wp-admin/includes/image.php';
        require_once ABSPATH . 'wp-admin/includes/file.php';
        require_once ABSPATH . 'wp-admin/includes/media.php';
        preg_match('/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $url, $matches);
        if (!$matches) {
            return new WP_Error('image_sideload_failed', __('Invalid image URL: ' . $url));
        }
        $file_array = array();
        $file_array['name'] = basename($matches[0]);
        $file_array['tmp_name'] = download_url($url);
        if (is_wp_error($file_array['tmp_name'])) {
            return $file_array['tmp_name'];
        }
        $id = media_handle_sideload($file_array, $post_id);
        if (is_wp_error($id)) {
            @unlink($file_array['tmp_name']);
            return $id;
        }
        return self::get_by_attachment_id($id);
    }
}
