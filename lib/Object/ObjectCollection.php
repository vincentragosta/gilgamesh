<?php

namespace Gilgamesh\Object;

use Gilgamesh\Collection\Collection;

/**
 * Class ObjectCollection
 * @package Gilgamesh\Object
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
abstract class ObjectCollection implements Collection
{
    protected $items;
    protected $items_hashmap;
    protected static $object_class_name = null;

    public function __construct(array $items)
    {
        $this->items = $items;
        $this->items = $this->items_hashmap = [];
        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    public function addItem($item)
    {
        $this->validateClass($item);
        if (!$this->hashExists($item)) {
            $this->items[] = $item;
            end($this->items);
            $this->items_hashmap[$this->getObjectHash($item)] = key($this->items);
        }
        return $this;
    }

    public function removeItem($item)
    {
        $this->validateClass($item);
        $index = $this->getHashIndex($item);
        if (false !== $index) {
            $item = $this->items[(int)$index];
            unset($this->items_hashmap[$this->getObjectHash($item)]);
            unset($this->items[(int)$index]);
        }
        return $this;
    }

    // Array Specific Helpers

    public function find($id)
    {
        $hash = $this->getHashFromId($id);
        return isset($this->items_hashmap[$hash]) ?
            $this->items[$this->items_hashmap[$hash]] : false;
    }

    public function walk(callable $callback)
    {
        $this->map($callback);
        return $this;
    }

    public function walkMethod(string $method_name, ...$method_args)
    {
        $this->mapMethod($method_name, ...$method_args);
        return $this;
    }

    public function map(callable $callback): array
    {
        return array_map($callback, $this->items);
    }

    public function mapMethod(string $method_name, ...$method_args): array
    {
        return $this->map(function ($item) use ($method_name, $method_args) {
            return $this->callMethodChain($item, $method_name, $method_args);
        });
    }

    public function filter(callable $callback)
    {
        $that = $this();
        foreach ($that->items as $item) {
            if (!$callback($item)) {
                $that->removeItem($item);
            }
        }
        return $that;
    }

    public function filterMethod(string $method_name, ...$method_args)
    {
        $that = $this();
        foreach ($that->items as $item) {
            if (!$that->callMethodChain($item, $method_name, $method_args)) {
                $that->removeItem($item);
            }
        }
        return $that;
    }

    public function reduce(callable $callback, $initial = null)
    {
        return array_reduce($this->items, $callback, $initial);
    }

    // Helpers

    public function count()
    {
        return count($this->items);
    }

    public function isEmpty()
    {
        return $this->count() == 0;
    }

    public function first()
    {
        return $this->items[0] ?? null;
    }

    public function last()
    {
        return $this->items[$this->count() - 1] ?? null;
    }

    public function getAll(): array
    {
        return $this->items;
    }

    //

    protected function validateClass($item)
    {
        if (!$item instanceof static::$object_class_name) {
            throw new \InvalidArgumentException('Object passed to collection is not a ' . static::$object_class_name);
        }
    }

    public function callMethodChain($item, $method_chain, $method_args)
    {
        $chain = array_reverse(explode('.', $method_chain));
        while (!empty($chain)) {
            $method_name = array_pop($chain);
            $item = call_user_func_array([$item, $method_name], count($chain) ? [] : $method_args);
        }
        return $item;
    }

    protected function hashExists($item)
    {
        return isset($this->items_hashmap[$this->getObjectHash($item)]);
    }

    protected function getHashIndex($item) {
        return $this->items_hashmap[$this->getObjectHash($item)] ?? false;
    }

    protected function getHashFromId($id)
    {
        return md5($id);
    }

    abstract protected function getObjectHash($item);

    public function __invoke()
    {
        return clone $this;
    }
}
