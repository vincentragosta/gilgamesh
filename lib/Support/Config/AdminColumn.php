<?php

namespace Gilgamesh\Support\Config;

use Gilgamesh\Image;

/**
 * Class AdminColumn
 * @package Gilgamesh\Support\Config
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class AdminColumn
{
    public function __construct()
    {
        add_filter('gilgamesh/admin_col', [$this, 'postMeta'], 5, 3);
        add_filter('gilgamesh/admin_col/thumbnail', [$this, 'postThumbnail'], 5, 2);
        add_filter('gilgamesh/admin_col/editor', [$this, 'editor'], 5, 2);
        add_filter('gilgamesh/admin_col/excerpt', [$this, 'excerpt'], 5, 2);
    }

    public function postMeta($content, $post_id, $column_id)
    {
        return get_post_meta($post_id, $column_id) ?: $content;
    }

    public function postThumbnail($content, $post_id)
    {
        if (!class_exists('Image')) {
            return $content;
        }
        $img = Image::get_featured($post_id);
        if (empty($img)) {
            return $content;
        }
        if ($img->height > 75) {
            $img->height(75);
        }

        return $img->get_html();
    }

    public function editor($content, $post_id)
    {
        $post = get_post($post_id);
        return strip_tags($post->post_content) ?: $content;
    }

    public function excerpt($content, $post_id)
    {
        $post = get_post($post_id);
        return strip_tags($post->post_excerpt) ?: $content;
    }
}
