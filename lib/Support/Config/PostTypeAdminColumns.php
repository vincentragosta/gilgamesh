<?php

namespace Gilgamesh\Support\Config;

/**
 * Class PostTypeAdminColumns
 * @package Gilgamesh\Support\Config
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class PostTypeAdminColumns
{
    protected $slug, $register, $extras, $admin_columns, $query_filter;

    public function __construct($slug, PostType $PostType, $extras = null)
    {
        $this->slug = $slug;
        $this->register = $PostType;
        $this->extras = $extras ?? [];
        $this->admin_columns = new PostTypeAdminColumn($this->slug, $this->register);
        $this->query_filter = new PostTypeSort($this->slug, $this->register);
        if (empty($this->extras['admin_columns'])) {
            $this->extras['admin_columns'] = [];
        }
        foreach ($this->extras as $key => $args) {
            $method_name = lcfirst(str_replace('_', '', ucwords($key, '_')));
            if (method_exists($this, $method_name)) {
                call_user_func([$this, $method_name], $args);
            }
        }
    }

    protected function defaultSort($args)
    {
        $this->query_filter->setDefaultSort($args);
    }

    protected function adminSort($args)
    {
        $this->query_filter->setAdminSort($args);
    }

    protected function titlePlaceholder($title)
    {
        if (empty($title) || !is_string($title)) {
            return;
        }
        add_filter('enter_title_here', function ($default, $post) use ($title) {
            return $post->post_type == $this->slug ? $title : $default;
        }, 10, 2);
    }

    protected function adminColumns($args)
    {
        new AdminColumn();
        $this->admin_columns->init($args);
        $this->query_filter->setColumns($args);
        new AdminFilters($this->slug, $args, $this->register);
    }

    protected function cmspoLabel($label)
    {
        $this->query_filter->cmspoLabel($label);
    }
}
