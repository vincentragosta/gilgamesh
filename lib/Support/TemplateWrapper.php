<?php

namespace Gilgamesh\Support;

/**
 * Class TemplateWrapper
 * @package Gilgamesh\Support
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class TemplateWrapper
{
    public static $main_template;
    public $slug;
    public $templates;
    public static $base;

    public function __construct($template = 'base.php')
    {
        $this->slug = basename($template, '.php');
        $this->templates = [$template];

        if (self::$base) {
            $str = substr($template, 0, -4);
            array_unshift($this->templates, sprintf($str . '-%s.php', self::$base));
        }
    }

    public function __toString()
    {
        $this->templates = apply_filters('wp-scaffold/theme/wrap_' . $this->slug, $this->templates);
        return locate_template($this->templates);
    }

    public static function wrap($main)
    {
        if (!is_string($main)) {
            return $main;
        }
        self::$main_template = $main;
        self::$base = basename(self::$main_template, '.php');

        if (self::$base === 'index') {
            self::$base = false;
        }

        return new TemplateWrapper();
    }

    public static function init()
    {
        add_filter('template_include', [__CLASS__, 'wrap'], 109);
    }

    public static function include()
    {
        include static::$main_template;
    }
}

