<?php

namespace Gilgamesh\User;

use Gilgamesh\DTO\DTO;

/**
 * Class UserDTO
 * @package ChildGilgamesh\User
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $id
 * @property string $display_name
 * @property string $user_email
 * @property string $user_login
 * @property string $user_nicename
 * @property string $user_photo_url
 * @property bool $is_verified
 */
class UserDTO extends DTO
{
    protected $id;
    protected $display_name;
    protected $user_email;
    protected $user_login;
    protected $user_nicename;
    protected $user_photo_url;
    protected $is_verified;

    public function __construct(User $User)
    {
        $this->id = $User->ID;
        $this->display_name = $User->getDisplayName();
        $this->user_email = $User->user_email;
        $this->user_login = $User->user_login;
        $this->user_nicename = $User->user_nicename;
        $this->user_photo_url = $User->getUserPhotoUrl();
        $this->is_verified = $User->isVerified();
    }
}
