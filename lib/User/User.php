<?php

namespace Gilgamesh\User;

use Gilgamesh\Post\Post;

/**
 * Class User
 * @package ChildGilgamesh\User
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class User extends \WP_User
{
    public static function createFromCurrentUser()
    {
        $WP_User = wp_get_current_user();
        if ($WP_User->ID == 0) {
            return null;
        }
        return new static($WP_User->ID);
    }

    public static function createFromAuthor()
    {
        $Author = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
        if ($Author->ID == 0 ) {
            return null;
        }
        return new static($Author->ID);
    }

    public static function createFromPost(Post $Post)
    {
        return $Post->post()->post_author ? new static($Post->post()->post_author) : null;
    }

    public static function createFromEmail(string $email)
    {
        if (!$User = get_user_by('email', $email)) {
            return false;
        }
        return new static($User);
    }

    public static function createFromUserLogin(string $user_login)
    {
        if (!$User = get_user_by('login', $user_login)) {
            return false;
        }
        return new static($User);
    }

    public static function createFromAdmin()
    {
        if (!$User = get_user_by('user_login', static::ADMIN_ACCOUNT)) {
            return false;
        }
        return new static($User);
    }

    public static function createFromUserId(string $user_id)
    {
        return new static($user_id);
    }

    public function permalink()
    {
        return home_url(sprintf('/user/%s', $this->user_login));
    }

    public function deleteField(string $field)
    {
        return delete_user_meta($this->ID, $field);
    }

    public function updateField(string $meta, string $value)
    {
        return update_user_meta($this->ID, $meta, $value);
    }

    public function getField(string $field, bool $single_value = true)
    {
        return get_user_meta($this->ID, $field, $single_value);
    }
}
