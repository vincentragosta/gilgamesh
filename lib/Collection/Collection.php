<?php

namespace Gilgamesh\Collection;

/**
 * Interface Collection
 * @package Gilgamesh\Collection
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @verrsion 1.0
 */
interface Collection
{
    public function addItem($item);

    public function removeItem($item);

    public function find(string $id);

    public function map(callable $callback);

    public function walk(callable $callback);

    public function walkMethod(string $name, ...$args);

    public function filter(callable $callback);

    public function filterMethod(string $name, ...$args);

    public function callMethodChain($item, $chain, $args);

    public function reduce(callable $callback, $initial);

    public function count();

    public function isEmpty();

    public function getAll();
}
