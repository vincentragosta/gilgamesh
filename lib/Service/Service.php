<?php

namespace Gilgamesh\Service;

/**
 * Class Service
 * @package Gilgamesh\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property array $items
 */
abstract class Service
{
    protected $items;

    public function __construct($items)
    {
        if (!is_array($items)) {
            $items = [$items];
        }
        $this->items = $items;
    }

    abstract function run();

    public function hasItems()
    {
        return !empty($this->getItems());
    }

    public function getItems()
    {
        return $this->items;
    }
}
