<?php

namespace Gilgamesh\Service;

use Gilgamesh\Support\Config\Taxonomy;
use Gilgamesh\Support\Config\PostType;
use Gilgamesh\Support\Config\PostTypeAdminColumns;

/**
 * Class RegisterConfigService
 * @package Gilgamesh\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class RegisterConfigService extends Service
{
    protected $config_pattern = TEMPLATEPATH . '/config/*.json';

    public function __construct(array $items = [])
    {
        parent::__construct($items);
        if (empty($this->items)) {
            $this->items = $this->getFiles();
        }
    }

    public function run()
    {
        foreach($this->items as $file) {
            if (!file_exists($file)) {
                continue;
            }
            if (empty($json_data = json_decode(file_get_contents($file), true))) {
                continue;
            }
            foreach($json_data as $type => $type_data) {
                foreach($type_data as $key => $data) {
                    switch($type) {
                        case 'custom_post_types':
                            $PostType = (new PostType($data['labels'], $data['args']));
                            if (!post_type_exists($key)) {
                                register_post_type($key, $PostType->args);
                            }
                            new PostTypeAdminColumns($key, $PostType, $data['extras']);
                            break;
                        case 'taxonomies':
                            $post_types = is_string($data['post_types']) ? [$data['post_types']] : $data['post_types'];
                            if (!taxonomy_exists($key)) {
                                register_taxonomy($key, $post_types, (new Taxonomy($data['labels'], $data['args']))->args);
                                foreach ($post_types as $post_type) {
                                    register_taxonomy_for_object_type($key, $post_type);
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    public function getFiles()
    {
        return glob($this->getConfigDirectory());
    }

    public function getConfigDirectory()
    {
        return $this->config_pattern;
    }
}
