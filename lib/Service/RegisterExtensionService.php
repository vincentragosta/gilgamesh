<?php

namespace Gilgamesh\Service;

/**
 * Class RegisterExtensionService
 * @package Gilgamesh\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property array $items
 */
final class RegisterExtensionService extends Service
{
    public function run()
    {
        if (!$this->hasItems()) {
            return;
        }
        foreach ($this->getItems() as $Extension) {
            if (!class_exists($Extension)) {
                continue;
            }
            if (method_exists($Extension = new $Extension(), 'register')) {
                $Extension->register();
            }
        }
    }
}
