<?php

namespace Gilgamesh\Social;

/**
 * Class SocialIcon
 * @package Gilgamesh\Social
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SocialIcon
{
    protected $icon_name;
    protected $url;

    public function __construct(string $icon_name, string $url)
    {
        $this->icon_name = $icon_name;
        $this->url = $url;
    }

    public static function createFromData(array $data)
    {
        return new static(strtolower($data['icon']), $data['url']);
    }

    public function getIconName()
    {
        return $this->icon_name;
    }

    public function getUrl()
    {
        return $this->url;
    }
}