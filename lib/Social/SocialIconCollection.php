<?php

namespace Gilgamesh\Social;

use Gilgamesh\Object\ObjectCollection;

/**
 * Class SocialIconCollection
 * @package Gilgamesh\Social
 * @author Vincent Ragosta <vincentpasqualeragosata@gmnail.com>
 * @version 1.0
 */
class SocialIconCollection extends ObjectCollection
{
    protected static $object_class_name = SocialIcon::class;

    public function __construct(array $items)
    {
        if (!$items[0] instanceof SocialIcon) {
            $items = array_map(function($item) {
                return SocialIcon::createFromData($item);
            }, $items);
        }
        parent::__construct($items);
    }

    protected function getObjectHash($item)
    {
        /** @var SocialIcon $item */
        return md5($item->getUrl());
    }
}