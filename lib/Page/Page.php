<?php

namespace Gilgamesh\Page;

use Gilgamesh\Post\Post;

/**
 * Class Page
 * @package Gilgamesh\Page
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Page extends Post
{
    const POST_TYPE = 'page';

    public function parent()
    {
        return $this->post()->post_parent ? static::create($this->post()->post_parent) : null;
    }

    public function template()
    {
        return get_page_template_slug($this->ID) ?: 'page.php';
    }
}